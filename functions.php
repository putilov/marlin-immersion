<?php
/**
 * @return PDO
 *
 * @description В связи с частым использованием в разных запросах - было принято решение вынести в отдельную функцию
 */
function pdo_db_connect(): PDO
{
    $servername = "localhost";
    $username = "mysql";
    $password = "mysql";
    $dbname = "marlin";

    return new PDO("mysql:host=$servername;dbname=$dbname", $username, $password );
}

/**
 * @param string $email
 * @param string $password
 * @return bool
 *
 * @description авторизация пользователя
 */
function login(string $email, string $password): bool
{
    $pdo = pdo_db_connect();
    $sql = "SELECT * FROM users WHERE email=:email";
    $statement = $pdo->prepare($sql);
    $statement->execute(['email' => $email]);
    $user = $statement->fetch();
    $is_user_verified = $user && password_verify($password, $user['password']);

    if (!$is_user_verified)
    {
        set_flash_message("danger", "Логин или пароль неверны!");
        
        return false;
    }

    user_auth($user);

    return true;
}

/**
 * @description Собственно logout
 */
function logout()
{
    unset($_SESSION["user_data"]);

    redirect_to("/page_login.php");
}

/**
 * @param array $user
 *
 * @description Авторизация пользователя
 */
function user_auth(array $user)
{
    $_SESSION["user_data"] = [
        "id" => $user["id"],
        "email" => $user["email"],
    ];
}

/**
 * @return bool
 *
 * @description проверяет залогинен ли пользователь
 */
function is_logged_in(): bool
{
    return isset($_SESSION["user_data"]) && !empty($_SESSION["user_data"]);
}

/**
 * @return array
 *
 * @description получение списка всех пользователей
 */
function get_users(): array
{
    $pdo = pdo_db_connect();
    $sql = "SELECT * FROM users";
    $statement = $pdo->prepare($sql);
    $statement->execute();

    return $statement->fetchAll(PDO::FETCH_ASSOC);
}

/**
 * @param string $field
 * @param $value
 * @return mixed
 *
 * @description в связи с повторением почти всего кода функций получения пользователя по email и по id - было решено объединить в одну функцию
 */
function get_user_by_field(string $field, $value): mixed
{
    $where_statement = "$field=:" . "$field";
    $pdo = pdo_db_connect();
    $sql = "SELECT * FROM users WHERE $where_statement";
    $statement = $pdo->prepare($sql);
    $statement->execute([$field => $value]);

    return $statement->fetch();
}

/**
 * @return bool
 */
function is_admin(): bool
{
    $user_id = $_SESSION["user_data"]["id"];
    $user = get_user_by_field("id", $user_id);

    return $user["role"] == "admin";
}

/**
 * @param string $email
 * @param string $password
 *
 * @return int
 * @description добавить пользователя в db
 * @question судя по видео нужно возвращать id. Не понял для чего возвращать id если в дальнейшем мы его не используем
 */
function add_user(string $email, string $password): int
{
    $pdo = pdo_db_connect();
    $sql = "INSERT INTO users (email, password) VALUES (:email, :password)";
    $statement = $pdo->prepare($sql);
    $statement->execute(['email' => $email, 'password' => password_hash($password, PASSWORD_DEFAULT)]);

    return $pdo->lastInsertId();
}

/**
 * @param string $key
 * @param string $message
 *
 * @description подготовить flash сообщение
 */
function set_flash_message(string $key, string $message)
{
    $_SESSION[$key] = $message;
}

/**
 * @param string $key
 *
 * @description вывести flash сообщение
 */
function display_flash_message(string $key)
{
    if(isset($_SESSION[$key]))
    {
        echo "<div class='alert alert-$key" . ($key == 'danger' ? ' text-dark' : '') ."' role='alert'>$_SESSION[$key]</div>";
        unset($_SESSION[$key]);
    }
}

/**
 * @param string $path
 *
 * @description перенаправить на другую страницу
 */
function redirect_to(string $path)
{
    header("Location: $path");
    exit;
}

/**
 * @param array $user
 * @param bool $is_lower_case
 * @return string
 *
 * @description Получаем имя пользователя. Если ни того ни другого нет - отдаю email
 * @description реализован функционал по приведению имени в нижний регистр для атрибута data-filter-tags
 */
function get_user_full_name(array $user, bool $is_lower_case = false): string
{
    $user_name = $user["user_name"];

    if($user_name)
    {
        return $is_lower_case ? strtolower($user_name) : $user_name;
    }

    return $user["email"];
}

/**
 * @param array $user
 * @return string
 *
 * @description получаем аватар. Если аватар в базу не добавлен - подставляю аватар основываясь на гендерной принадлежности пользователя.
 * @description По умолчанию Male (генерируется в бд)
 */
function get_user_avatar(array $user): string
{
    $avatar = $user["avatar_url"];
    $sex = substr($user["sex"], 0, 1);

    if($avatar)
    {
        return $avatar;
    }

    return ("img/demo/avatars/avatar-" . $sex . ".png");
}

/**
 * @param int $user_id
 * @return bool
 */
function can_edit_user(string $user_id): bool
{
    return is_admin() || $user_id === $_SESSION["user_data"]["id"];
}

/**
 * @param int $user_id
 *
 * @description В базе данных меняем основную информацию пользователя
 */
function edit_user_information(int $user_id)
{
    $user_name = $_POST["user_name"];
    $company = $_POST["company"];
    $phone_number = $_POST["phone_number"];
    $address = $_POST["address"];

    $pdo = pdo_db_connect();
    $sql = "UPDATE users SET user_name=:user_name, company=:company, phone_number=:phone_number, address=:address WHERE id=:user_id";
    $statement = $pdo->prepare($sql);
    $statement->execute(["user_name" => $user_name, "company" => $company, "phone_number" => $phone_number, "address" => $address, "user_id" => $user_id]);
}

/**
 * @param int $user_id
 *
 * @description В базе данных меняем статус пользователя
 */
function edit_user_status(int $user_id)
{
    $status = $_POST["status"];

    $pdo = pdo_db_connect();
    $sql = "UPDATE users SET status=:status WHERE id=:user_id";
    $statement = $pdo->prepare($sql);
    $statement->execute(["status" => $status, "user_id" => $user_id]);
}

/**
 * @param int $user_id
 *
 * @description В базе данных меняем социальные линки
 */
function edit_user_social_links(int $user_id)
{
    $vk_link = $_POST["vk_link"];
    $telegram_link = $_POST["telegram_link"];
    $instagram_link = $_POST["instagram_link"];

    $pdo = pdo_db_connect();
    $sql = "UPDATE users SET vk_link=:vk_link, telegram_link=:telegram_link, instagram_link=:instagram_link WHERE id=:user_id";
    $statement = $pdo->prepare($sql);
    $statement->execute(["vk_link" => $vk_link, "telegram_link" => $telegram_link, "instagram_link" => $instagram_link, "user_id" => $user_id]);
}

/**
 * @param int $user_id
 * @return mixed
 *
 * @description Получаем текущую ссылку на аватар пользователя или null если таковой нет
 */
function current_user_avatar(int $user_id): array|null
{
    $pdo = pdo_db_connect();
    $sql = "SELECT * FROM users WHERE id=:id";
    $statement = $pdo->prepare($sql);
    $statement->execute(['id' => $user_id]);
    $user = $statement->fetch();

    return $user["avatar_url"];
}

/**
 * @param int $user_id
 *
 * @description Редактируем аватар пользователя, предварительно подчищая старый аватар из файлов
 */
function edit_user_avatar(int $user_id)
{
    $path = $_FILES['avatar']['name'];
    $ext = pathinfo($path, PATHINFO_EXTENSION);
    $upload_dir = 'img/demo/avatars/';
    $new_uniq_image_name = uniqid("avatar_",false);
    $final_name = $upload_dir . $new_uniq_image_name . "." . $ext;

    $user_avatar = current_user_avatar($user_id);

    if($user_avatar && file_exists($user_avatar))
    {
        unlink($user_avatar);
    }

    if(move_uploaded_file($_FILES['avatar']['tmp_name'], $final_name))
    {
        $pdo = pdo_db_connect();
        $sql = "UPDATE users SET avatar_url=:avatar_url WHERE id=:id";
        $statement = $pdo->prepare($sql);
        $statement->execute(["avatar_url" => $final_name, 'id' => $user_id]);
    }
}

/**
 * @param string|null $status
 * @return string
 *
 * @description Генерируем класс для статуса в зависимости от статуса пользоваля из db
 */
function status_class_list(string|null $status)
{
    if(!$status)
    {
        return "mr-3";
    }

    $CLASS_LIST = "status mr-3 status-";

    switch ($status)
    {
        case "away":
            return $CLASS_LIST . "warning";
        case "busy":
            return $CLASS_LIST . "danger";
    }

    return $CLASS_LIST . "success";
}