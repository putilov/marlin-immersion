<?php
session_start();

require "functions.php";

// Проверяю, если пользователь попал на эту страницу без POST запроса с нужными данными - возвращаю его на страницу регистрации
if(!isset($_POST["email"]) || !isset($_POST["password"]))
{
    redirect_to("/page_register.php");
}

// Получаю с глобальной переменной $_POST значения полей email и password
$email = $_POST["email"];
$password = $_POST["password"];

$user = get_user_by_field("email", $email);

if(!empty($user)){
    set_flash_message("danger", "<strong>Уведомление!</strong> Этот эл. адрес уже занят другим пользователем.");
    redirect_to("/page_register.php");
}

add_user($email, $password);
//user_auth($user);
set_flash_message("success", "Регистрация успешна");
redirect_to('/page_login.php');