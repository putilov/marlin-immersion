<?php
session_start();

require "functions.php";

// Проверяю, если пользователь попал на эту страницу без POST запроса с нужными данными - возвращаю его на страницу авторизации
if(!isset($_POST["email"]) || !isset($_POST["password"]))
{
    redirect_to("/page_login.php");
}

// Получаю с глобальной переменной $_POST значения полей email и password
$email = $_POST["email"];
$password = $_POST["password"];

$is_logged_in = login($email, $password);

redirect_to($is_logged_in ? "/users.php" : "/page_login.php");





