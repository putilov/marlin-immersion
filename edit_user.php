<?php
session_start();

require "functions.php";



$user_id = $_POST["editable_user_id"] ?? null;

if(!isset($_POST) || !$user_id)
{
    redirect_to("/users.php");
}

edit_user_information($user_id);

set_flash_message("success", "Профиль успешно изменен!");
redirect_to("/page_profile.php?id=" . $user_id);
