<?php
session_start();

require "functions.php";

if(!isset($_POST["email"]) || !isset($_POST["password"]))
{
    redirect_to("/users.php");
}

$email = $_POST["email"];
$password = $_POST["password"];

$user = get_user_by_field("email", $email);

if(!empty($user)){
    set_flash_message("danger", "<strong>Уведомление!</strong> Этот эл. адрес уже занят другим пользователем.");
    redirect_to("/create_user.php");
}

$user_id = add_user($email, $password);

edit_user_information($user_id);
edit_user_status($user_id);
edit_user_social_links($user_id);
edit_user_avatar($user_id);

set_flash_message("success", "Пользователь успешно добавлен!");
redirect_to("/users.php");